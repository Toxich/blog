<?php require_once(ROOT_PATH."/templates/partials/header.php");?>

<main role="main" class="back">

    <div class="container pb-5 pt-5">
    <div class="container album pb-3">

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="inner cover text-center">
                    <div class="h1 inner mt-3 mb-3 font-weight-normal">Choose custom search properties :</div>

                    <div class="card-body">
                        <form class="form" method="POST" action="/search/customResults">
                            <?php if (!empty($_SESSION['error']['post'])): ?>
                                <div class="alert alert-danger" role="alert">
                                    <a> <?php echo($_SESSION['error']['post']) ?> </a>
                                    <?php unset($_SESSION['error']['post']) ?>
                                </div>
                            <?php endif; ?>

                            <input type="hidden" name="_token" value="7xr38A2zuKL0dmU6gNj7xt1R8ZoAmSpIUvmxpjre">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Search</label>
                                <div class="col-md-6">
                                    <input id="text" class="form-control " name="key" value="" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Category</label>
                                <select name="category" class="form-control col-md-6 " required>
                                    <?php foreach ($data['categories'] as $key): ?>
                                        <option value="<?php echo ($key['id']) ?>" > <?php echo($key['title']) ?> </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Subcategory</label>
                                <select name="subcategory" class="form-control col-md-6 " required>
                                    <?php foreach ($data['subcategories'] as $key): ?>
                                        <option value="<?php echo ($key['id']) ?>" > <?php echo($key['title']) ?> </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Date</label>
                                <div class="col-md-6">
                                    <input id="text" class="form-control " name="date" value=""  autofocus>
                                </div>
                            </div>

                            <div class="form-group row mb-0 p-4">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary float-right">
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    </main>

    <script src="<?php echo SITE_URL; ?>/js/post.js"></script>

<?php require_once(ROOT_PATH."/templates/partials/footer.php");?>