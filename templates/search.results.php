<?php require_once(ROOT_PATH."/templates/partials/header.php");?>

<main role="main" class="back">

    <h1 class="p-4 text-center" >Search results :</h1>
      <div class=" py-5 ">
        <div class="container">
            <span>Haven't found what you need? Try <a href="http://blog.loc/search/custom" >custom search</a>  </span>
            <?php if(empty($data['posts'])): ?>
            <div class="p-5 row album rounded text-center" >
                <h2 class="p-4 " >Sorry, nothing found</h2>
            </div>
            <?php else: ?>
                    <div class="p-5 row album rounded" >
                        <?php foreach ($data['posts'] as $key): ?>
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <h4><?php echo($key['heading']) ?></h4>
                                        <p class="card-text"><?php echo($key['text']) ?> </p>
                                        <span class="text-muted"> <?php echo($key['title']) ?></span>
                                        <hr>
                                        <span class="text-muted">by <?php echo($key['name']) ?></span>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group pt-2">
                                                <a type="button" value="<?php echo($key['id']) ?>" href="/post/show/<?php echo($key['id']) ?>"  class="btn btn-sm btn-outline-secondary " >
                                                    View
                                                </a>
                                            </div>
                                            <small class="text-muted">9 mins</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>

            <?php endif; ?>
          </div>
        </div>
      </div>


    </main>

    <script src="<?php echo SITE_URL; ?>/js/post.js"></script>

<?php require_once(ROOT_PATH."/templates/partials/footer.php");?>