<?php require_once(ROOT_PATH . "/templates/partials/header.php"); ?>


<main class="back">
    <div class="container  pt-5 pb-5">

        <div class="row album">

            <!-- Post Content Column -->
            <div class="col-lg-8">

                <!-- Title -->
                <h1 class="mt-4"><?php echo($data['post']['heading']) ?></h1>
                <!-- Author -->
                <p class="lead">
                    by
                    <a href="#"><?php echo($data['post']['name']) ?></a>
                </p>
                <hr>
                <!-- Date/Time -->
                <p><?php echo($data['post']['created_at']) ?></p>
                <hr>
                <!-- Post Content -->
                <p class="lead"><?php echo($data['post']['text']) ?></p>

                <hr>
                <div class="container row">
                    <button type="button" class="btn <?php if($data['liked']){ echo('liked'); }else{echo ('notliked');} ?> " id="like"  >
                    </button>
                    <span class="pl-2 pt-3" id="likes-count" ><?php echo($data['likes']) ?></span>
                </div>
                <hr>
                <div class="comments">

                <?php foreach ($data['comments'] as $key): ?>

                    <div class="media mb-4 comment" id="<?php echo($key['id']) ?>">
                        <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                        <div class="media-body">
                            <h5 class="mt-0"><?php echo($key['name']) ?></h5>
                            <?php echo($key['text']) ?>
                        </div>
                        <?php if (isset($_SESSION['user']) && $key['user_id'] == $_SESSION['user']): ?>
                            <button type="button" class="btn btn-sm btn-outline-secondary delete_comment"
                                    value="<?php echo($key['id']) ?>">Delete
                            </button>
                        <?php endif; ?>
                    </div>

                <?php endforeach ?>

                </div>

                <?php if (isset($_SESSION['user'])): ?>
                    <div class="card-body">
                        <form class="form" method="POST">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Comment</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="text" id="text"
                                              aria-label="With textarea"></textarea>
                                </div>
                            </div>

                            <div class="form-group row mb-0 p-4">
                                <div class="col-md-8 offset-md-4">
                                    <button type="button" value="<?php echo($_SESSION['user']) ?>" class="btn btn-primary float-left"
                                            id="post-comment">
                                        Post
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>


<script src="<?php echo SITE_URL; ?>/js/comment.js"></script>

<?php require_once(ROOT_PATH . "/templates/partials/footer.php"); ?>
