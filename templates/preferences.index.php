<?php require_once(ROOT_PATH . "/templates/partials/header.php"); ?>
<main>
    <div class="container">
        <h1>Choose your interests</h1>
        <form class="form" method="POST" action="preferences/save" >
            <div class="row justify-content-center p-3">
                <div class="col-sm-4">
                    <?php foreach ($data['tree'] as $r): ?>
                        <?php echo $r; ?>
                    <?php endforeach; ?>
                </div>

            </div>
            <div class="form-group row mb-0 p-4">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary float-right">
                        Create
                    </button>
                </div>
            </div>
        </form>
    </div>
</main>


<script src="<?php echo SITE_URL; ?>/js/register.js"></script>

<?php require_once(ROOT_PATH . "/templates/partials/footer.php"); ?>
