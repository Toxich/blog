<?php require_once(ROOT_PATH . "/templates/partials/header.php"); ?>
<main>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="inner cover text-center">
                    <div class="h1 inner mt-3 mb-3 font-weight-normal">Login</div>

                    <div class="card-body">
                        <form class="form-signin" method="POST" action="/login/check">
                                <?php if(!empty($_SESSION['error']['login'])): ?>
                                <div class="alert alert-danger" role="alert">
                                    <a> <?php echo($_SESSION['error']['login']) ?> </a>
                                    <?php unset($_SESSION['error']['login']) ?>
                                </div>
                                <?php endif; ?>

                            <?php if(!empty($_SESSION['message'])): ?>
                                <div class="alert alert-success" role="alert">
                                    <a> <?php echo($_SESSION['message']) ?> </a>
                                    <?php unset($_SESSION['message']) ?>
                                </div>
                            <?php endif; ?>

                            <input type="hidden" name="_token" value="7xr38A2zuKL0dmU6gNj7xt1R8ZoAmSpIUvmxpjre">
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control " name="email" value="" required
                                           autocomplete="email" autofocus>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control " name="password" required
                                           autocomplete="current-password">

                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php require_once(ROOT_PATH . "/templates/partials/footer.php"); ?>
