<?php require_once(ROOT_PATH . "/templates/partials/header.php"); ?>

<?php include(ROOT_PATH . "/templates/partials/modals.php");?>


    <div role="main" class="back">

        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit post</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body edit-modal-body">
                        <form class="form">
                            <input type="hidden" name="_token" value="7xr38A2zuKL0dmU6gNj7xt1R8ZoAmSpIUvmxpjre">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Recipient:</label>
                                <input type="text" name="heading" class="form-control " id="heading-edit">
                                <input type="hidden" name="id" class="form-control" id="id-edit">

                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Message:</label>
                                <textarea class="form-control" name="text" id="text-edit"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="editPost">Edit post</button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>

        <!-- Modal end -->

        <div class="album py-5 bg-light">
            <div class="container">

                <div class="row ">

                    <?php foreach ($data['posts'] as $key): ?>
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <div class="card-body">
                                    <h4><?php echo($key['heading']) ?></h4>
                                    <p class="card-text"><?php echo($key['text']) ?> </p>
                                    <span class="text-muted">by <?php echo($key['name']) ?></span>
                                    <div class="d-flex justify-content-between align-items-center ">
                                        <div class="btn-group pt-2">
                                            <button type="button" value="<?php echo($key['id']) ?>" data-toggle="modal"
                                                    data-target="#showModal"
                                                    class="btn btn-sm btn-outline-secondary show">
                                                View
                                            </button>
                                            <?php if (!empty($_SESSION['user'])): ?>
                                                <button type="button" value="<?php echo($key['id']) ?>"
                                                        data-toggle="modal" data-target="#editModal"
                                                        class="btn btn-sm btn-outline-secondary form-show">
                                                    Edit
                                                </button>
                                                <button type="button" value="<?php echo($key['id']) ?>"
                                                        class="btn btn-sm btn-outline-secondary delete-post">
                                                    Delete
                                                </button>
                                            <?php endif; ?>
                                        </div>
                                        <small class="text-muted">9 mins</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>

                </div>
            </div>
            <div class="p-5">
                <a type="button" href="/post/create" class=" btn btn-primary btn-lg float-right">New post</a>
            </div>
        </div>
    </div>


    </main>

    <script src="<?php echo SITE_URL; ?>/js/post.js"></script>
<?php require_once(ROOT_PATH . "/templates/partials/footer.php"); ?>