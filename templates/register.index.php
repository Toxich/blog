<?php require_once(ROOT_PATH . "/templates/partials/header.php"); ?>
<main>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="inner cover text-center">
                    <div class="h1 inner mt-3 mb-3 font-weight-normal">Registration</div>

                    <div class="card-body">
                        <form class="form-signin" method="POST" action="">

                            <?php if(!empty($_SESSION['error']['register'])): ?>
                                <div class="alert alert-danger" role="alert">
                                    <a> <?php echo($_SESSION['error']['register']) ?> </a>
                                    <?php unset($_SESSION['error']['register']) ?>
                                </div>
                            <?php endif; ?>

                            <input type="hidden" name="_token" value="7xr38A2zuKL0dmU6gNj7xt1R8ZoAmSpIUvmxpjre">

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="name" class="form-control " name="name" value="" required
                                           autocomplete="name" autofocus>

                                </div>
                            </div>
                            <div class="alert alert-danger" id="error-email-exists">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>Error! </strong> User with this email is already registered.
                            </div>
                            <div class="alert alert-danger" id="error-email-invalid">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>Error! </strong> Email is invalid
                            </div>

                            <div class="form-group row">

                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control " name="email" value="" required
                                           autocomplete="email" autofocus>

                                </div>
                            </div>

                            <div class="alert alert-danger" id="error-password-invalid">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>Error! </strong> Password must contain at least 1 uppercase, 1 lowercase, 1 digit, length at least 8 characters.
                            </div>

                            <div class="form-group row">

                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control " name="password" required
                                           autocomplete="current-password">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button  id="register" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<script src="<?php echo SITE_URL; ?>/js/register.js"></script>

<?php require_once(ROOT_PATH . "/templates/partials/footer.php"); ?>
