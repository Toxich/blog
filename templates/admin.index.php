<?php require_once(ROOT_PATH . "/templates/partials/header.php"); ?>


<main>
    <div class="container ">
        <h1 class="p-4 text-center">Posts table</h1>
        <div class="border mt-4 p-3 ">
            <table id="posts" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Post heading</th>
                    <th>Text</th>
                    <th>Author</th>
                    <th>Likes</th>
                    <th>Updated at</th>
                    <th>Functions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data['posts'] as $key): ?>
                    <tr id="<?php echo($key['id']) ?>">
                        <td><?php echo($key['id']) ?></td>
                        <td><?php echo($key['heading']) ?></td>
                        <td><?php echo($key['text']) ?></td>
                        <td><?php echo($key['name']) ?></td>
                        <td><?php echo($key['likes']) ?></td>
                        <td><?php echo($key['updated_at']) ?></td>
                        <td>
                            <button class="btn btn-primary delete" value="<?php echo($key['id']) ?>">
                                Delete
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</main>


<script src="<?php echo SITE_URL; ?>/js/admin.js"></script>

<?php require_once(ROOT_PATH . "/templates/partials/footer.php"); ?>
