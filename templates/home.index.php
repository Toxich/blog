<?php require_once(ROOT_PATH."/templates/partials/header.php");?>

<?php include(ROOT_PATH . "/templates/partials/modals.php");?>


<main role="main" class="back">
    <?php if(empty($_SESSION['user'])): ?>
      <section   class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading">Album example</h1>
          <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.</p>
          <p>
            <a href="/register" class="btn btn-primary my-2">Sign up</a>
            <a href="/login" class="btn btn-secondary my-2">Login</a>
          </p>
        </div>
      </section>
    <?php else: ?>
        <?php if(!empty($data['preferences'])): ?>
            <div class=" pt-4 container text-center ">
                <h1 class="jumbotron-heading">Your preferences are :</h1>
                <p class="font-weight-bold">
                    <?php
                        $i = 0;
                        $count = count($data['preferences']);
                        foreach ($data['preferences'] as $preferences):
                             ?>
                    <?php
                        $i++;
                        if($i != $count){
                            echo($preferences['title'].', ');
                        } else {
                            echo($preferences['title'] . '. ');
                        }
                        ?>
                    <?php endforeach; ?>
                </p>
            </div>
        <?php else : ?>
            <section   class="jumbotron text-center">
                <div class="container">
                    <h1 class="jumbotron-heading">Choose your preferences</h1>
                    <p class="lead text-muted"> To see posts related to your preferences you should choose them. </p>
                    <p>
                        <a href="/preferences" class="btn btn-primary my-2">Choose</a>
                    </p>
                </div>
            </section>
        <?php endif; ?>
    <?php endif; ?>
    <h1 class="p-4 text-center" >Last posts :</h1>
      <div class=" py-5 ">
        <div class="container">
            <?php if(empty($_SESSION['user']) || empty($data['preferences'])): ?>
                <div id="all" class="tab-pane ">
                    <div class="p-5 row album rounded" role="tabpanel" aria-labelledby="main-tab">
                        <?php foreach ($data['posts'] as $key): ?>
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <h4><?php echo($key['heading']) ?></h4>
                                        <p class="card-text"><?php echo($key['text']) ?> </p>
                                        <span class="text-muted"> <?php echo($key['title']) ?></span>
                                        <hr>
                                        <span class="text-muted">by <?php echo($key['name']) ?></span>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group pt-2">
                                                <a type="button" value="<?php echo($key['id']) ?>" href="/post/show/<?php echo($key['id']) ?>"  class="btn btn-sm btn-outline-secondary " >
                                                    View
                                                </a>
                                            </div>
                                            <small class="text-muted">9 mins</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php else: ?>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item active my-tab border">
                    <a class="nav-link active"  data-toggle="tab" href="#home" role="tab" aria-controls="main" aria-selected="true">Home</a>
                </li>
                <li class="nav-item my-tab rounded border">
                    <a class="nav-link" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="false">All posts</a>
                </li>
                <li class="nav-item my-tab rounded border">
                    <a class="nav-link" data-toggle="tab" href="#preferences" role="tab" aria-controls="all" aria-selected="false">by Preferences</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <!--        Home posts sorted by parent id and user preferences        -->
                <div id="home" class="tab-pane in active">
                      <div class="p-5 row album rounded" role="tabpanel" aria-labelledby="main-tab">
                          <?php foreach ($data['home_posts'] as $key): ?>
                              <div class="col-md-4">
                                  <div class="card mb-4 box-shadow">
                                      <div class="card-body">
                                          <h4><?php echo($key['heading']) ?></h4>
                                          <p class="card-text"><?php echo($key['text']) ?> </p>
                                          <span class="text-muted"> <?php echo($key['title']) ?></span>
                                          <hr>
                                          <span class="text-muted">by <?php echo($key['name']) ?></span>
                                          <div class="d-flex justify-content-between align-items-center">
                                              <div class="btn-group pt-2">
                                                  <a type="button" value="<?php echo($key['id']) ?>" href="/post/show/<?php echo($key['id']) ?>"  class="btn btn-sm btn-outline-secondary " >
                                                      View
                                                  </a>
                                              </div>
                                              <small class="text-muted">9 mins</small>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          <?php endforeach ?>
                      </div>
                </div>
                <!--        All posts from newest to oldest        -->
                <div id="all" class="tab-pane ">
                    <div class="p-5 row album rounded" role="tabpanel" aria-labelledby="main-tab">
                        <?php foreach ($data['all_posts'] as $key): ?>
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <h4><?php echo($key['heading']) ?></h4>
                                        <p class="card-text"><?php echo($key['text']) ?> </p>
                                        <span class="text-muted"> <?php echo($key['title']) ?></span>
                                        <hr>
                                        <span class="text-muted">by <?php echo($key['name']) ?></span>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group pt-2">
                                                <a type="button" value="<?php echo($key['id']) ?>" href="/post/show/<?php echo($key['id']) ?>"  class="btn btn-sm btn-outline-secondary " >
                                                    View
                                                </a>
                                            </div>
                                            <small class="text-muted">9 mins</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>

                <div id="preferences" class="tab-pane">
                    <div class="p-5 row album rounded" role="tabpanel" aria-labelledby="main-tab">
                        <?php foreach ($data['preferences_posts'] as $key): ?>
                            <div class="col-md-4">
                                <div class="card mb-4 box-shadow">
                                    <div class="card-body">
                                        <h4><?php echo($key['heading']) ?></h4>
                                        <p class="card-text"><?php echo($key['text']) ?> </p>
                                        <span class="text-muted"> <?php echo($key['title']) ?></span>
                                        <hr>
                                        <span class="text-muted">by <?php echo($key['name']) ?></span>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group pt-2">
                                                <a type="button" value="<?php echo($key['id']) ?>" href="/post/show/<?php echo($key['id']) ?>"  class="btn btn-sm btn-outline-secondary " >
                                                    View
                                                </a>
                                            </div>
                                            <small class="text-muted">9 mins</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>

            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>


    </main>

    <script src="<?php echo SITE_URL; ?>/js/post.js"></script>

<?php require_once(ROOT_PATH."/templates/partials/footer.php");?>