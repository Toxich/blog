<!-- Modal Text -->
<div class="modal fade showModal" id="showModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body view-modal-body">
                <form class="form">
                    <input type="hidden" name="_token" value="7xr38A2zuKL0dmU6gNj7xt1R8ZoAmSpIUvmxpjre">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Heading:</label>
                        <input type="text" name="heading" class="form-control "  readonly id="heading-show">
                        <input type="hidden" name="id" class="form-control modal-edit" id="id-show">

                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Text:</label>
                        <textarea class="form-control" name="text" readonly id="text-show"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    </div>
                </form>
            </div>

        </div>

    </div>
</div>




<!-- Modal end -->