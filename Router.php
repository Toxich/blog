<?php

namespace Blog;

class Router
{
    private $routes = [];

    public function setRoute($controller, $action, $id = null)
    {
        if (!empty($this->routes[$controller])) {
            if (!in_array($action, $this->routes[$controller])) {
                $this->routes[$controller][] = $action;
            }
        } else {
            $this->routes[$controller][] = $action;
        }
        if(!empty($id)){
            $this->routes[$controller][$action][] = $id;
        }
    }

    public function run($query)
    {
        if ($query !== "/") {
            $parts = explode("/", $query);
            $controller = $parts[1] ?? '';
            $action = $parts[2] ?? 'index';
            $id = $parts[3] ?? '';
        }
        if (empty($controller) && empty($action)) {
            $controller = "home";
            $action = "index";
        }


        if (empty($action) || !isset($this->routes[$controller]) || !in_array($action, $this->routes[$controller])) {
            header("HTTP/1.0 404 Not Found");
            return;
        }
        $view = new \Blog\App\Viewers\Viewer($controller, $action, ROOT_PATH);
        $controllerName = '\\Blog\\App\\Controllers\\' . ucfirst($controller) . "Controller";
        $controllerObj = new $controllerName($view);
        if(isset($id)){
            $controllerObj->{$action . "Action"}($id);
        }else{
        $controllerObj->{$action . "Action"}($_REQUEST);}
    }
}
