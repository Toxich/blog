<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define("ROOT_PATH", dirname(__FILE__, 2));
define("SITE_URL", "http://localhost");
define("DB_USER", "root");
define("DB_PASS", "root");
define("DB_NAME", "blog");
define("SALT", "etrdeyd373xs");
session_start();

require_once(ROOT_PATH."/vendor/autoload.php");

use \Blog\Router;
use \Blog\App\Models\Base\DB;

ob_start();
ob_end_clean();
DB::connect(DB_NAME, DB_USER, DB_PASS);
$router = new Router();
$router->setRoute("home", "index");
$router->setRoute("login", "index");
$router->setRoute("register", "index");
$router->setRoute("register", "validate");
$router->setRoute("register", "save");
$router->setRoute("register", "sendConfirmation");
$router->setRoute("register", "confirmation", '\b[0-9a-f]{5,40}\b');
$router->setRoute("login", "check");
$router->setRoute("login", "logout");
$router->setRoute("login", "test");
$router->setRoute("main", "index");
$router->setRoute("main", "editheading");
$router->setRoute("main", "edittext");
$router->setRoute("post", "create");
$router->setRoute("post", "save");
$router->setRoute("post", "edit");
$router->setRoute("post", "delete");
$router->setRoute("post", "commentDelete");
$router->setRoute("post", "commentSave");
$router->setRoute("post", "show", '[0-9]+');
$router->setRoute("post", "getComments");
$router->setRoute("post", "addComment");
$router->setRoute("post", "like");
$router->setRoute("post", "deleteLike");
$router->setRoute("post", "sendNotification");
$router->setRoute("admin", "index");
$router->setRoute("admin", "postDelete");
$router->setRoute("preferences", "index");
$router->setRoute("preferences", "save");
$router->setRoute("search", "results");
$router->setRoute("search", "custom");
$router->setRoute("search", "customResults");
$router->setRoute("home", "test");

$router->run($_SERVER['REQUEST_URI']);
?>
