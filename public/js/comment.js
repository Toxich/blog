let success;
var comments;
let postId;
let url;
let commentsOnPage = [];
let commentId;

function save(userId, postId, text, event) {
    $.ajax({
        url: "/post/commentSave",
        type: "POST",
        dataType: 'html',
        data: {userId: userId, postId: postId, text: text},
        success: function (data) {
            console.log("success");
            $(".comments").append(data);
            success = 1;
            commentId = $('.comments').children('.comment').last().attr('id');
            console.log("id = " + commentId);
            $.ajax({
                url: "/post/sendNotification",
                type: "POST",
                dataType: 'json',
                data: {postId: postId, url: url, commentId: commentId},
                success: function (data) {
                    console.log("success");
                    console.log(data);
                },
                error: function (data) {
                    console.log("ERROR")
                }
            });

        },
        error: function (data) {
            event.preventDefault();
            alert("ERROR");
        }
    });
}

function deleteComment(commentId) {
    $.ajax({
        url: "/post/commentDelete",
        type: "POST",
        dataType: 'json',
        data: {commentId: commentId},
        async: false,
        success: function (data) {
            console.log("success");
            $('#' + commentId).remove();
            lastOnPage = $('.comments').children('.comment').last().attr('id');
            getLast();
        },
        error: function (data) {
            console.log("ERROR")
        }
    });

}


function updateComments(id) {
    $.ajax({
        url: "/post/addComment",
        type: "POST",
        dataType: 'html',
        data: {id: id},
        success: function (data) {
            console.log("success");
            $(".comments").append(data);
            success = 1;
        },
        error: function (data) {

            alert("ERROR");
        }
    });
}

function getComments() {
    $.ajax({
        url: "/post/getComments",
        type: "POST",
        dataType: 'json',
        data: {postId: postId},
        success: function (data) {
            comments = data;
            console.log(comments);
            $(".comments").find(".comment").each(function () {
                commentsOnPage.push(this.id);
            });
            if ($(commentsOnPage).not(comments).length === 0 && $(comments).not(commentsOnPage).length === 0) {
                console.log('OK');
            } else {
                $.each(commentsOnPage, (function (key, value) {
                    if (jQuery.inArray(value, comments) === -1) {
                        console.log("not in Array" + value);
                        $('#' + value).remove();
                        console.log("REMOVE");

                    }
                }));
                $.each(comments, function (key, value) {
                    if (jQuery.inArray(value, commentsOnPage) === -1) {
                        console.log("New comment" + value);
                        updateComments(value);
                    }
                })

            }
            console.log(commentsOnPage);
            commentsOnPage = [];
        },
        error: function (data) {
            alert("ERROR");
        }
    });
}

function like(event) {
    $.ajax({
        url: "/post/like",
        type: "POST",
        dataType: 'html',
        data: {postId: postId},
        success: function (data) {
            var likes = $("#likes-count").text();
            if (data == -1) {
                redirectTo();
            } else if (likes > data) {
                $("#likes-count").text(data);
                $("#like").removeClass("liked");
                $("#like").addClass("notliked");
            } else if(likes < data){
                $("#likes-count").text(data);
                $("#like").removeClass("notliked");
                $("#like").addClass("liked");
            }
        },
        error: function (data) {
            alert("ERROR");
        }
    });
}

function redirectTo() {
    window.location.href = '/login'
}

$(document).ready(function () {

     url = $(location).attr('href'),
        parts = url.split("/"),
        last_part = parts[parts.length - 1];
    postId = last_part;

    $("#post-comment").click(function (event) {
        var userId = $(this).val();

        var text = $('#text').val();
        console.log(userId);
        console.log(last_part);
        console.log(text);
        if(text) {
            save(userId, postId, text)
        }else {
            event.preventDefault();
            alert("Enter a comment");
        }
    });

    $("#like").click(function (event) {
        like(event);
    });

    setInterval("getComments()", 1000 * 10);

});

$(document).on("click", ".delete_comment", function (event) {
    var commentId = $(this).val()
    console.log(commentId);
    deleteComment(commentId);

});