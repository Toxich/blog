let postId;

function deletePost() {
    $.ajax({
        url: "/admin/postDelete",
        type: "POST",
        dataType: 'json',
        data: {postId: postId},
        async: false,
        success: function (data) {
            console.log("success")
            $('#' + postId).remove();
        },
        error: function (data) {
            console.log("ERROR")
        }
    });

}

$(document).ready(function () {
    $('#posts').DataTable();
    $('.delete').click(function (event) {
        postId = $(this).val()
        console.log(postId)
        deletePost();
    });
});