let heading;
let text;


function getHeading (id) {
    $.ajax({
        url:"/main/editheading",
        type: "POST",
        dataType: 'json',
        data: {id: id},
        async: false,
        success:function(result){
            heading = result
        },
        error: function (result) {
            console.log("ERROR")
        }
    });
}

function getText (id) {
    $.ajax({
        url:"/main/edittext",
        type: "POST",
        dataType: 'json',
        data: {id: id},
        async: false,
        success:function(result){
            text = result
        },
        error: function (result) {
            console.log("ERROR")
        }
    });
}

$(document).ready(function () {
     $(".form-show").click(function () {
         console.log('edit')
         var id = $(this).val()
         getHeading(id)
         getText(id)
         $('#editModal').find('.edit-modal-body #heading-edit').val(heading)
        $('#editModal').find('.edit-modal-body #text-edit').val(text)
        $('#editModal').find('.edit-modal-body #id-edit').val(id)

     });

    $(".show").click(function () {
        console.log('show')
        var id = $(this).val()
        getHeading(id)
        getText(id)
        $('#showModal').find('.view-modal-body #heading-show').val(heading)
        $('#showModal').find('.view-modal-body #text-show').val(text)
        $('#showModal').find('.view-modal-body #id-show').val(id)

    });

    $(".delete-post").click(function () {
        var id = $(this).val()
        $.ajax({
            url:"/post/delete",
            type: "POST",
            dataType: 'json',
            data: {id: id},
            async: false,
            success:function(data){
                console.log("success")
                location.reload();
            },
            error: function (data) {
                console.log("ERROR")
            }
        });

    });

    $('#editModal').on('click', '#editPost', function (e) {
        var id = $("#id-edit").val()
        heading = $("#heading-edit").val()
        text = $("#text-edit").val()
        console.log(id)
        console.log(heading)
        console.log(text)
        $.ajax({
            url:"/post/edit",
            type: "POST",
            dataType: 'json',
            data: {id: id, heading: heading, text: text},
            async: false,
            success:function(data){
                console.log("success")
               location.reload();
            },
            error: function (data) {
                console.log("ERROR")
            }
        });
    });


});

