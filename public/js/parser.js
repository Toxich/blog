const puppeteer = require('puppeteer');
const mysql = require('mysql');

var con = mysql.createConnection({
    host: "mysql",
    user: "root",
    password: "root",
    database: "blog"
});



async function getData(url){
    try {
        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        });
    const page = await browser.newPage();
    await page.goto(url);
    const headings = await page.evaluate(() => {
  //      return document.getElementsByClassName('category-news');
        let elements = Array.from(document.querySelectorAll('#content > div.post.block-in-loop.category-news > div > div.col-xs-8.col-txt > h2'));
        return elements.map(element => {
            return element.innerText
        })
    });

    const texts = await page.evaluate(() => {
        let elements = Array.from(document.querySelectorAll('#content > div.post.block-in-loop.category-news > div > div.col-xs-8.col-txt > div.entry-excerpt.hidden-xs'));
        return elements.map(element => {
            return element.innerText
        })
    });

    for(let i = 0; i < headings.length; i++){
        var headingData = headings[i];
        var textData = texts[i];

        var sql = ('INSERT INTO news (heading, text) VALUES ('+ ' \' ' + headingData + ' \' ' + ' ,  ' + ' \' ' + textData + ' \' ' + ' ) ');
        con.query(sql, function (err, result) {
            if (err) throw err;
            console.log("1 record inserted");
        });
    }
    con.end();

    await browser.close();
    return headings;
    } catch(e) {
        console.log(e);
    }
}


const url = 'https://itc.ua/';
getData(url)
    .then();