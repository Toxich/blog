let validation;
let regResult;
let sendResult;


function validateEmail(sEmail, event) {
    var filter = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
    if (filter.test(sEmail)) {
        checkEmail(sEmail)
        if (validation) {
            $("#error-email-exists").show();
            event.preventDefault();
            alert("ERROR");
            return false;
        }else {
            return true;
        }
    }
    else {
        $("#error-email-invalid").show();
        return false;
    }
}

function validatePass(password) {
    var filter = (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/);
    if (filter.test(password)){
        return true
    }else{
        $("#error-password-invalid").show();
        return false
    }
}

function checkEmail(email) {
    $.ajax({
        url: "/register/validate",
        type: "POST",
        dataType: 'json',
        data: {email: email},
        async: false,
        success: function (data) {
            console.log("OK MAILLL");
            validation = data;
        },
        error(data){
            console.log("FAIL MAILLL");
        }
    });
}

function redirect(data) {
    console.log("REDIRECT");
    window.location.href = '/login';
}

function save(name, email, password) {
    $.ajax({
        url: "/register/save",
        type: "POST",
        dataType: 'json',
        data: {name: name, email: email, password: password},
        success: function (data) {
            console.log("OK");
            redirect(data);
        },
        error(data){
            console.log("REG FAAAAILLL");
        }
    });
}

function send(email) {
    $.ajax({
        url: "/register/sendConfirmation",
        type: "POST",
        dataType: 'json',
        data: {email: email},
        async: false,
        success: function (data) {
            console.log("OK send");
            sendResult = data;
        },
        error(data) {
            console.log("FAIL SEEEND");

        }
    });
}


$(document).ready(function () {
    $("#error-email-exists").hide();
    $("#error-email-invalid").hide();
    $("#error-password-invalid").hide();
    $('#register').click(function (event) {
        var name = $("#name").val();
        var email = $("#email").val();
        var password = $("#password").val();
        if(validateEmail(email, event) && validatePass(password, event)){
            send(email);
            if(sendResult != 0){
                save(name,email,password)
            }else {
                alert("Wrong email");
                event.preventDefault();
            }
        }else {
            event.preventDefault();
        }


    });
});