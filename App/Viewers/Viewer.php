<?php

namespace Blog\App\Viewers;

class Viewer
{
    const FOLDER = "/templates";

    public $template = '';
    private $templatePath = '';

    public function __construct($controller, $action, $dir)
    {
        $this->template = $controller . "." . $action . ".php";
        $this->templatePath = $dir . self::FOLDER;
    }

    public function render($data = null)
    {
        require $this->templatePath . "/" . $this->template;
    }
}
