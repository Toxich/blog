<?php
namespace Blog\App\Commands;

use Blog\App\Models\Post;
use Blog\App\Models\User;
use Blog\App\Models\Preferences;
use PHPMailer\PHPMailer\PHPMailer;

$userClass = new User();
$preferencesClass = new Preferences();
$postsClass = new Post();
$users = $userClass->getUsers();
foreach ($users as $user) {
    $preferences = $preferencesClass->getPreferencesList($user['id']);
    foreach ($preferences as $preference) {
        $preferencesId[] = $preference['id'];
    }
    $posts = $postsClass->getDailyPosts($preferencesId);
    $link = SITE_URL.'/post/show/';
    $text = '';
    foreach ($posts as $post) {
        $text .= "\n New post on your category " . $post['title'] . " named " . $post['heading'] . " : " . $link . "" . $post['id'];
        $text .= "<br>";
    }

    $mail = new PHPMailer(); // create a new object
    $mail->IsSMTP();
    $mail->SMTPSecure = 'tls';// enable SMTP
    // $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 465; // or 587 465
    $mail->IsHTML(true);
    $mail->Username = "toxich.chepurenko@gmail.com";
    $mail->Password = "megatoxich228";
    $mail->SetFrom("toxich.chepurenko@gmail.com");
    $mail->Subject = "Notification about posts";
    $mail->Body = $text;
    $mail->AddAddress($user['mail']);
}