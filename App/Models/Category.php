<?php

namespace Blog\App\Models;

use Blog\App\Models\Base\DB;


class Category
{
    protected $id;
    protected $parentId;
    protected $category;

    public function __construct($id = null, $parentId = null, $category = null)
    {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->category = $category;
    }

    public function fetchCategoryTreeList($preferences = null, $parent = 0, $userTreeArray = '')
    {
        if (!is_array($userTreeArray)) {
            $userTreeArray = [];
        }

        $stmt = DB::$conn->query("SELECT * FROM `categories` WHERE parent_id = $parent ORDER BY id ASC");
        $result = $stmt->fetchAll();
        $i = 0;
        if (count($result) > 0) {
            $userTreeArray[] = "<ul >";
            foreach ($result as $row) {
                if($parent != 0) {
                    $i++;
                    $userTreeArray[] = "<li class=' list-group-item '>
                                            <div class=\"chiller_cb\">";
                    if($preferences != null && in_array($row['id'], $preferences)) {
                        $userTreeArray[] = "<input name=" . $row['id'] . " value= " . $row['id'] . " id=\"myCheckbox" . $row['id'] . "\" type=\"checkbox\" checked > ";
                    }else{
                        $userTreeArray[] = "<input name=" . $row['id'] . " value= " . $row['id'] . " id=\"myCheckbox" . $row['id'] . "\" type=\"checkbox\" > ";
                    }
                    $userTreeArray[] =         "<label for=\"myCheckbox".$row['id']."\" >" . $row['title'] . "</label>
                                                <span></span>
                                             </div>
                                         </li>";

                }else{
                    $userTreeArray[] = "<li style+='decoreation: none'>
                                             <h3>" . $row['title'] . " :</h3>
                                        </li>";
                }
                $userTreeArray = self::fetchCategoryTreeList($preferences, $row["id"], $userTreeArray);
            }
            $userTreeArray[] = "</ul><br/>";
        }
        return $userTreeArray;
    }

    public function getSubCategories(){
        $stmt = DB::$conn->query("SELECT * FROM `categories` WHERE parent_id != 0 ");
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getCategories(){
        $stmt = DB::$conn->query("SELECT * FROM `categories` WHERE parent_id = 0 ");
        $result = $stmt->fetchAll();
        return $result;
    }

}