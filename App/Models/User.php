<?php

namespace Blog\App\Models;

use Blog\App\Models\Base\DB;


class User
{
    protected $id;
    protected $mail;
    protected $password;

    public function __construct($id = null, $mail = null, $password = null)
    {
        $this->id = $id;
        $this->mail = $mail;
        $this->password = $password;
    }

    public function getName($id)
    {
        $stmt = DB::$conn->query("SELECT name FROM users WHERE id = '$id' ");
        $result = $stmt->fetchAll();
        return $result[0]['name'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getUsers()
    {
        $stmt = DB::$conn->query("SELECT * FROM users");
        return $stmt->fetchAll();
    }

    public function save($mail, $password, $name, $hash)
    {
        $stmt = DB::$conn->prepare(
            "INSERT INTO users(`mail`, `password`, `name`, `hashmail`) VALUES(:mail, :password,:name, :hash)"
        );
        $stmt->execute(['mail' => $mail, 'password' => $password, 'name' => $name, 'hash' => $hash]);
        $stmt = DB::$conn->query("SELECT id FROM users WHERE mail = '$mail' ");
        $result = $stmt->fetchAll();
        return $result;
    }

    public function confirmed($hash)
    {
        $stmt = DB::$conn->query("SELECT id FROM users WHERE hashmail = '$hash'");
        $result = $stmt->fetchAll();
        if ($result != null) {
            $stmt = DB::$conn->prepare(
                "UPDATE `users` SET  `active`= 1 WHERE `hashmail`='$hash' "
            );
            $result = $stmt->execute();
            return $result;
        } else {
            return false;
        }
    }

    public function checkActive($id)
    {
        $stmt = DB::$conn->query("SELECT active FROM users WHERE id = '$id'");
        $active = $stmt->fetchAll();
        return $active;
    }

    public function check($mail, $password)
    {
        var_dump($mail);
        var_dump($password);
        $stmt = DB::$conn->query("SELECT id FROM users WHERE mail = '$mail' AND password = '$password' ");
        var_dump($stmt);
        $result = $stmt->fetchAll();
        if (!empty($result)) {
            return $result[0]['id'];
        } else {
            return false;
        }
    }

    public function checkUser($mail)
    {
        $stmt = DB::$conn->query("SELECT id FROM users WHERE mail = '$mail' ");
        $result = $result = $stmt->fetch();

        if (!empty($result)) {
            return $result[0]['id'];
        } else {
            return false;
        }
    }

    public function checkAdmin($id)
    {
        $stmt = DB::$conn->query("SELECT admin FROM users WHERE id ='$id'  ");
        $admin = $stmt->fetch();
        return $admin['admin'];
    }

}