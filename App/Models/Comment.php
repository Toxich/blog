<?php

namespace Blog\App\Models;

use Blog\App\Models\Base\DB;


class Comment
{
    protected $id;
    protected $heading;
    protected $text;

    public function __construct($id = null, $heading = null, $text = null)
    {
        $this->id = $id;
        $this->heading = $heading;
        $this->text = $text;
    }


    public function getText($id)
    {
        $stmt = DB::$conn->query("SELECT text FROM comments WHERE post_id = '$id'  ");
        return $text = $stmt->fetch();
    }

    public function getComments($id)
    {
        $stmt = DB::$conn->query(
            "SELECT name, text, comments.id, comments.updated_at, user_id 
             FROM `comments` LEFT JOIN users 
             ON comments.user_id = users.id WHERE comments.post_id = '$id' "
        );
        return $result = $stmt->fetchAll();
    }


    public function save($userId, $postId, $text)
    {
        $stmt = DB::$conn->prepare(
            "INSERT INTO comments(`post_id`, `user_id`, `text`) VALUES(:post_id,:user_id, :text)"
        );
        $stmt->execute(['post_id' => $postId, 'user_id' => $userId, 'text' => $text]);
        $this->id = DB::$conn->lastInsertId();
        return $this->id;
    }

    public function edit($id, $heading, $text)
    {
        $stmt = DB::$conn->prepare(
            "UPDATE `posts` SET  `heading`='$heading', `text`='$text' WHERE `id`='$id' "
        );
        $stmt->execute();
        return true;
    }

    public function delete($id)
    {
        $stmt = DB::$conn->prepare(
            "DELETE FROM `comments` WHERE `id`='$id' "
        );
        $stmt->execute();
        return true;
    }

    public function getComment($id)
    {
        $stmt = DB::$conn->query(
            "SELECT name, text, comments.updated_at, user_id 
             FROM `comments` LEFT JOIN users 
             ON comments.user_id = users.id WHERE comments.id = '$id' "
        );
        return $result = $stmt->fetch();
    }

    public function updateComments($postId){
        $stmt = DB::$conn->query(
            "SELECT id FROM comments WHERE post_id = '$postId' "
        );

        return $stmt->fetchAll();
    }

}