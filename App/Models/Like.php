<?php

namespace Blog\App\Models;

use Blog\App\Models\Base\DB;


class Like
{
    protected $id;
    protected $userId;
    protected $postId;

    public function __construct($id = null, $userId = null, $postId = null)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->postId = $postId;
    }

    public function getLikes($postId)
    {
        $stmt = DB::$conn->query(
            "SELECT id
             FROM `likes`  WHERE post_id = '$postId' "
        );
        return $result = $stmt->fetchAll();
    }

    public function getLikedPost($postId, $userId)
    {
        $stmt = DB::$conn->query(
            "SELECT id
             FROM `likes`  WHERE post_id = '$postId' && user_id = '$userId' "
        );
        return $result = $stmt->fetch();
    }

    public function addLike($postId, $userId)
    {
        $stmt = DB::$conn->prepare(
            "INSERT INTO likes(`post_id`, `user_id`) VALUES(:post_id,:user_id)"
        );
        $stmt->execute(['post_id' => $postId, 'user_id' => $userId]);
        return DB::$conn->lastInsertId();
    }

    public function delete($postId, $userId)
    {
        $stmt = DB::$conn->prepare(
            "DELETE FROM `likes` WHERE `post_id`='$postId' AND `user_id` = '$userId' "
        );
        $stmt->execute();
        return true;
    }

}