<?php

namespace Blog\App\Models;

use Blog\App\Models\Base\DB;


class Post
{
    protected $id;
    protected $heading;
    protected $text;

    public function __construct($id = null, $heading = null, $text = null)
    {
        $this->id = $id;
        $this->heading = $heading;
        $this->text = $text;
    }

    public function getHeading($id)
    {
        $stmt = DB::$conn->query("SELECT heading FROM posts WHERE id = '$id'  ");
        return $heading = $stmt->fetch();
    }

    public function getPost($id)
    {
        $stmt = DB::$conn->query("SELECT * FROM posts WHERE id = '$id'  ");
        return $heading = $stmt->fetch();
    }

    public function getText($id)
    {
        $stmt = DB::$conn->query("SELECT text FROM posts WHERE id = '$id'  ");
        return $text = $stmt->fetch();
    }

    public function getPosts()
    {
        $stmt = DB::$conn->query("SELECT title, name, text, heading, posts.id, user_id, updated_at, parent_id
             FROM `posts` LEFT JOIN categories
             ON posts.category_id = categories.id ORDER BY id DESC ");
        return $result = $stmt->fetchAll();
    }

    public function getPostsSortedByPreferences($preferences)
    {

        $preferencesString = join(', ', $preferences);
        $stmt = DB::$conn->query("SELECT title, name, text, heading, posts.id, user_id, updated_at, parent_id
             FROM `posts` LEFT JOIN categories
             ON posts.category_id = categories.id ORDER BY FIELD(parent_id, $preferencesString) desc");

        return $result = $stmt->fetchAll();
    }

    public function getPostsByPreferences($preferences)
    {
        $preferencesString = join(', ', $preferences);
        $stmt = DB::$conn->query("SELECT title, name, text, heading, posts.id, user_id, updated_at, parent_id
             FROM `posts` LEFT JOIN categories
             ON posts.category_id = categories.id where category_id IN ($preferencesString)");

        return $result = $stmt->fetchAll();
    }

    public function getUserPosts($id)
    {
        $stmt = DB::$conn->query("SELECT * FROM posts WHERE user_id = '$id'  ");
        return $result = $stmt->fetchAll();
    }

    public function getAuthorEmail($postId)
    {
        $stmt = DB::$conn->query(
            "SELECT mail, users.id 
             FROM `users` LEFT JOIN posts
             ON posts.user_id = users.id WHERE posts.id = '$postId' "
        );
        return $stmt->fetch();
    }


    public function save($userId, $heading, $text, $name, $categoryId)
    {
        $stmt = DB::$conn->prepare(
            "INSERT INTO posts(`user_id`, `heading`, `text`, `name`, `category_id`) VALUES(:user_id,:heading, :text, :name, :category_id)"
        );
        $stmt->execute(['user_id' => $userId, 'heading' => $heading, 'text' => $text, 'name' => $name, 'category_id' => $categoryId]);
        $this->id = DB::$conn->lastInsertId();
        return $this->id;
    }

    public function edit($id, $heading, $text)
    {

        $stmt = DB::$conn->prepare(
            "UPDATE `posts` SET  `heading`='$heading', `text`='$text' WHERE `id`='$id' " );
        $stmt->execute();
        return true;
    }
    public function delete($id)
    {
        $stmt = DB::$conn->prepare(
            "DELETE FROM `posts` WHERE `id`='$id' " );
        $stmt->execute();
        return true;
    }

    public function search($key, $preferences = null){
        if($preferences){
            $preferencesString = join(', ', $preferences);
            $stmt = DB::$conn->query("
                SELECT title, name, text, heading, posts.id, user_id, updated_at, parent_id
                FROM `posts` LEFT JOIN categories
                ON posts.category_id = categories.id WHERE concat(text,heading) LIKE '%$key%' 
                ORDER BY FIELD(parent_id, $preferencesString) desc
                ");
        }else {
            $stmt = DB::$conn->query("
                SELECT title, name, text, heading, posts.id, user_id, updated_at, parent_id
                FROM `posts` LEFT JOIN categories
                ON posts.category_id = categories.id WHERE concat(text,heading) LIKE '%$key%'
                ORDER BY id DESC
                ");
        }
        return $result = $stmt->fetchAll();
    }

    public function customSearch($key, $category, $subCategory, $date = null, $preferences = null){
        if($preferences){
            $preferencesString = join(', ', $preferences);
            $stmt = DB::$conn->query("
                SELECT title, name, text, heading, posts.id, user_id, updated_at, parent_id
                FROM `posts` LEFT JOIN categories
                ON posts.category_id = categories.id WHERE concat(text,heading) LIKE '%$key%'
                AND parent_id = '$category'
                AND category_id = '$subCategory'
                AND created_at LIKE '%$date%'
                ORDER BY FIELD(parent_id, $preferencesString) desc
                ");
        }else {
            $stmt = DB::$conn->query("
                SELECT title, name, text, heading, posts.id, user_id, updated_at, parent_id
                FROM `posts` LEFT JOIN categories
                ON posts.category_id = categories.id WHERE concat(text,heading) LIKE '%$key%'
                AND parent_id = '$category'
                AND category_id = '$subCategory'
                AND created_at LIKE '%$date%'
                ORDER BY id DESC
                ");
        }
        return $result = $stmt->fetchAll();
    }

    public function getDailyPosts($preferences)
    {
        $preferencesString = join(', ', $preferences);
        $stmt = DB::$conn->query("SELECT title, heading, posts.id, user_id
             FROM `posts` LEFT JOIN categories
             ON posts.category_id = categories.id 
             where category_id IN ($preferencesString) 
             AND `created_at` > timestampadd(day, -1, now())
             ");

        return $result = $stmt->fetchAll();
    }

}