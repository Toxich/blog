<?php

namespace Blog\App\Models;

use Blog\App\Models\Base\DB;


class Preferences
{
    protected $id;
    protected $userId;
    protected $category;

    public function __construct($id = null, $userId = null, $category = null)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->category = $category;
    }

    public function save($userId, $categoryId)
    {
        $stmt = DB::$conn->prepare(
            "INSERT INTO preferences(`user_id`, `category`) VALUES(:user_id, :category)"
        );
        $stmt->execute(['user_id' => $userId, 'category' => $categoryId]);
        $this->id = DB::$conn->lastInsertId();
        return $this->id;
    }

    public function getUserPreferences($userId)
    {
        $stmt = DB::$conn->query(
            "SELECT * FROM `preferences` WHERE user_id = '$userId' "
        );
        return $result = $stmt->fetchAll();
    }

    public function getPreferencesList($userId)
    {
        $stmt = DB::$conn->query(
            "SELECT title, categories.id, parent_id
             FROM categories LEFT JOIN preferences 
             ON categories.id = preferences.category WHERE preferences.user_id = '$userId'"
        );
        return $result = $stmt->fetchAll();
    }

    public function deletePreferences($id){
        $stmt = DB::$conn->prepare(
            "DELETE FROM `preferences` WHERE `id`='$id' "
        );
        $stmt->execute();
        return true;
    }


}