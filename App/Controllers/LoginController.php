<?php

namespace Blog\App\Controllers;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\Post;
use Blog\App\Models\Preferences;
use Blog\App\Models\User;
use PHPMailer\PHPMailer\PHPMailer;

class LoginController extends Controller
{

    public function indexAction($request = null)
    {
        $this->view->render();
    }

    public function checkAction()
    {
        $mail = $_POST['email'];
        $password = $_POST['password'];
        $password = sha1($password . SALT);
        $user = new User();
        $preferences = new Preferences();
        $_SESSION['user'] = $user->check($mail, $password);
        if ($_SESSION['user']) {
            $active = $user->checkActive($_SESSION['user']);
            if ($active[0]['active'] != 1) {
                $_SESSION['error']['login'] = 'Email is not confirmed yet';
                unset($_SESSION['user']);
                header("Location: /login");
                return;
            } else {
                $_SESSION['username'] = $user->getName($_SESSION['user']);
                if ($user->checkAdmin($_SESSION['user'])) {
                    $_SESSION['admin'] = 1;
                    header("Location: /admin");
                    return;
                } else {
                    if (empty($preferences->getUserPreferences($_SESSION['user']))) {
                        header("Location: /preferences");
                        return;
                    } else {
                        header("Location: /");
                        return;
                    }
                }
            }
        } else {
            $_SESSION['error']['login'] = 'Invalid credentials';
            header("Location: /login");
            return;
        }
    }

    public function logoutAction()
    {
        if (isset($_SESSION)) {
            session_destroy();
            header("Location: /login");
        }
    }




}