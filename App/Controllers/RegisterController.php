<?php

namespace Blog\App\Controllers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\User;

class RegisterController extends Controller
{

    public function indexAction($request = null)
    {
        $this->view->render();
    }

    public function saveAction()
    {
        $name = $_POST['name'];
        $mail = $_POST['email'];
        $hash = sha1($_POST['email'].SALT);
        $password = $_POST['password'];
        $password = sha1($password . SALT);
        $user = new User();
        $result = $user->save($mail, $password, $name, $hash);
        if (!empty($result)) {
            echo json_encode(1);
        } else {
            echo json_encode(0);
        }
    }

    public function validateAction(){
        $mail = $_POST['email'];
        $user = new User();
        $result = $user->checkUser($mail);
        if(!empty($result)){
            echo json_encode(1);
        }else{
            echo json_encode(0);
        }

    }

    public function sendConfirmationAction(){
        $address = $_POST['email'];
        $link = sha1($address.SALT);
        $mail = new PHPMailer(); // create a new object
        $mail->IsSMTP();
        $mail->SMTPSecure = 'tls';// enable SMTP
     //   $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587 465
        $mail->IsHTML(true);
        $mail->Username = "toxich.chepurenko@gmail.com";
        $mail->Password = "password";
        $mail->SetFrom("toxich.chepurenko@gmail.com");
        $mail->Subject = "Test";
        $mail->Body = "Your confirmation link is : ".SITE_URL."/register/confirmation/".$link;
        $mail->AddAddress($address);

        if(!$mail->Send()) {
            echo json_encode(0);
        } else {
            echo json_encode(1);
        }

    }

    public function confirmationAction($id){
        $user = new User();
        if($user->confirmed($id)){
            $_SESSION['message'] = "Email have been successfully confirmed";
            header("Location: /login");
        }else{
            header("HTTP/1.0 404 Not Found");
            return;
        }
    }


}