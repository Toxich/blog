<?php

namespace Blog\App\Controllers;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\Category;
use Blog\App\Models\Like;
use Blog\App\Models\User;
use Blog\App\Models\Post;
use Blog\App\Models\Comment;
use PHPMailer\PHPMailer\PHPMailer;

class PostController extends Controller
{

    public function createAction()
    {
        $categories = new Category();
        $result = $categories->getSubCategories();
        $this->view->render(['categories' => $result]);
    }

    public function saveAction()
    {
        $heading = $_POST['heading'];
        $text = $_POST['text'];
        $categoryId = $_POST['category'];
        $post = new Post();
        $result = $post->save($_SESSION['user'], $heading, $text, $_SESSION['username'], $categoryId);
        if (!empty($result)) {
            header("Location: /main");
        } else {
            header("HTTP/1.0 404 Not Found");
            return;
        }
    }

    public function editAction()
    {
        $id = $_POST['id'];
        $heading = $_POST['heading'];
        $text = $_POST['text'];
        $post = new Post();
        $result = $post->edit($id, $heading, $text);
        if ($result) {
            echo true;
        } else {
            echo false;
        }
    }

    public function deleteAction()
    {
        $id = $_POST['id'];
        $post = new Post();
        $result = $post->delete($id);
        if ($result) {
            echo true;
        } else {
            echo false;
        }
    }

    public function showAction($id)
    {
        $postClass = new Post();
        $commentClass = new Comment();
        $likes = new Like();
        $post = $postClass->getPost($id);
        if ($post == null) {
            header("HTTP/1.0 404 Not Found");
            return;
        }
        $comments = $commentClass->getComments($id);
        $likesPost = count($likes->getLikes($id));
        $isLiked = 0;
        if (isset($_SESSION['user'])) {
            if ($likes->getLikedPost($id, $_SESSION['user'])) {
                $isLiked = 1;
            }
        }
        $post['created_at'] = date('g:ia \o\n l jS F Y', strtotime($post['created_at']));
        $this->view->render(['post' => $post, 'comments' => $comments, 'likes' => $likesPost, 'liked' => $isLiked]);
    }

    public function commentSaveAction()
    {
        $id = $_POST['userId'];
        $postId = $_POST['postId'];
        $text = $_POST['text'];
        $comment = new Comment();
        $result = $comment->save($id, $postId, $text);
        $content = ($this->appendAction($result, $text, $id));
        echo $content;
    }

    public function commentDeleteAction()
    {
        $id = $_POST['commentId'];
        $comment = new Comment();
        $result = $comment->delete($id);
        if ($result) {
            echo $result;
        } else {
            echo false;
        }
    }

    public function getCommentsAction()
    {
        $postId = $_POST['postId'];
        $comment = new Comment();
        $commentsId = $comment->updateComments($postId);
        $result = [];
        foreach ($commentsId as $key) {
            $result[] = $key['id'];
        }
        echo json_encode($result);
    }

    public function addCommentAction()
    {
        $id = $_POST['id'];
        $comment = new Comment();
        $result = $comment->getComment($id);
        $content = ($this->appendAction($id, $result['text'], $result['user_id']));
        echo $content;
    }


    public function appendAction($commentId, $text, $id)
    {
        $user = new User();
        $name = $user->getName($id);
        $content = "";
        $content .= "<div class=\"media mb-4 comment\" id=\"$commentId\">";
        $content .= "<img class=\"d-flex mr-3 rounded-circle\" src=\"http://placehold.it/50x50\" alt=\"\">";
        $content .= "<div class=\"media-body\">";
        $content .= "<h5 class=\"mt-0\">$name</h5>";
        $content .= $text;
        $content .= "</div>";
        if ($id == $_SESSION['user'] && isset($_SESSION['user'])) {
            $content .= "<button type=\"button\" class=\"btn btn-sm btn-outline-secondary delete_comment\" value=\"$commentId\">Delete </button>";
        }
        $content .= "</div>";

        echo $content;
    }

    public function likeAction()
    {
        $postId = $_POST['postId'];
        if (!isset($_SESSION['user'])) {
            echo json_encode(-1);
            return;
        } else {
            $like = new Like();
            $ifLiked = $like->getLikedPost($postId, $_SESSION['user']);
            if ($ifLiked) {
                $result = $like->delete($postId, $_SESSION['user']);
                if ($result) {
                    $likesPost = count($like->getLikes($postId));
                    echo json_encode($likesPost);
                    return;
                }
            }
            $result = $like->addLike($postId, $_SESSION['user']);
            if ($result) {
                $likesPost = count($like->getLikes($postId));
                echo json_encode($likesPost);
            }
        }
    }

    public function sendNotificationAction(){
        $postId = $_POST['postId'];
        $link = $_POST['url'];
        $commentId = $_POST['commentId'];
        $post = new Post();
        $info = $post->getAuthorEmail($postId);
        $text = '';
        for($i = 0; $i < 3; $i++){
            $text .= "\n You have new comment on : " . $link ."#". $commentId;
            $text .= "<br>";
        }

        if($info['id'] != $_SESSION['user']) {
            $mail = new PHPMailer(); // create a new object
            $mail->IsSMTP();
            $mail->SMTPSecure = 'tls';// enable SMTP
            //   $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587 465
            $mail->IsHTML(true);
            $mail->Username = "toxich.chepurenko@gmail.com";
            $mail->Password = "megatoxich228";
            $mail->SetFrom("toxich.chepurenko@gmail.com");
            $mail->Subject = "Notification";
            $mail->Body = "\n You have new comment on : " . $link ."#". $commentId;
            $mail->AddAddress($info['mail']);

            if (!$mail->Send()) {
                echo json_encode(0);
            } else {
                echo json_encode(1);
            }
        }else{
            echo json_encode(false);
        }
    }
}