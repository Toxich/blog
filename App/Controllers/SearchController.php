<?php

namespace Blog\App\Controllers;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\Category;
use Blog\App\Models\Preferences;
use Blog\App\Models\User;
use Blog\App\Models\Post;

class SearchController extends Controller
{

    public function resultsAction()
    {
        if (isset($_POST)) {
            $post = new Post();
            $key = $_POST['key'];
            $preferencesClass = new Preferences();
            if (isset($_SESSION['user']) && !empty(
                $preferences = $preferencesClass->getPreferencesList(
                    $_SESSION['user']
                )
                )) {
                foreach ($preferences as $var) {
                    $preferencesId[] = $var['parent_id'];
                }
                $results = $post->search($key, $preferencesId);
                $this->view->render(["posts" => $results]);
                return;
            } else {
                $results = $post->search($key);
                $this->view->render(["posts" => $results]);
                return;
            }
        } else {
            header("Location: / ");
        }
    }


    public function customAction()
    {
        $categoriesClass = new Category();
        $categories = $categoriesClass->getCategories();
        $subCategories = $categoriesClass->getSubCategories();

        $this->view->render(['categories' => $categories, 'subcategories' => $subCategories]);
    }

    public function customResultsAction()
    {
        if (isset($_POST)) {
            $post = new Post();
            $preferencesClass = new Preferences();
            $key = $_POST['key'];
            $category = $_POST['category'];
            $subCategory = $_POST['subcategory'];
            if (isset($_POST['date'])) {
                $date = $_POST['date'];
            }
            if (isset($_SESSION['user']) && !empty(
                $preferences = $preferencesClass->getPreferencesList(
                    $_SESSION['user']
                )
                )) {
                foreach ($preferences as $var) {
                    $preferencesId[] = $var['parent_id'];
                }
                $results = $post->customSearch($key, $category, $subCategory, $date, $preferencesId);
                $this->view->render(["posts" => $results]);
                return;
            } else {
                $results = $post->customSearch($key, $category, $subCategory, $date);
                $this->view->render(["posts" => $results]);
                return;
            }


        } else {
            header("Location: / ");
        }
    }

}