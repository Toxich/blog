<?php

namespace Blog\App\Controllers;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\Preferences;
use Blog\App\Models\User;
use Blog\App\Models\Category;

class PreferencesController extends Controller
{

    public function indexAction($request = null)
    {
        $category = new Category();
        $preference = new Preferences();
        foreach ($preference->getUserPreferences($_SESSION['user']) as $preference) {
            $preferences[] = $preference['category'];
        }
        if(isset($preferences)){
            $tree = $category->fetchCategoryTreeList($preferences);
        }else{
            $tree = $category->fetchCategoryTreeList();
        }
        $this->view->render(['tree' => $tree]);
    }

    public function saveAction()
    {
        $preferences = new Preferences();
        if($check = $preferences->getUserPreferences($_SESSION['user'])){
            foreach ($check as $key){
                $preferences->deletePreferences($key['id']);
            }
            foreach ($_POST as $categoryId){
                $result[] = $preferences->save($_SESSION['user'], $categoryId);
            }
        }else{
            foreach ($_POST as $categoryId){
                $result[] = $preferences->save($_SESSION['user'], $categoryId);
            }
        }

        if(isset($result)){
            header("Location: /");
        }else{
            header("HTTP/1.0 404 Not Found");
        }
    }



}