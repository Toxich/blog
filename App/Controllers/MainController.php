<?php

namespace Blog\App\Controllers;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\User;
use Blog\App\Models\Post;

class MainController extends Controller
{

    public function indexAction($request = null)
    {
        if (!isset($_SESSION['user'])) {
            header("Location: /home");
        }
        $post = new Post();
        $posts = $post->getUserPosts($_SESSION['user']);
        $this->view->render(["posts" => $posts]);
    }

    public function saveAction()
    {
        $mail = $_POST['email'];
        $password = $_POST['password'];
        $password = sha1($password . SALT);
        $user = new User();
        $result = $user->save($mail, $password);
        if (!empty($result)) {
            header("Location: /login");
        } else {
        }
    }

    public function editheadingAction(){
        $id = $_POST['id'];
        $post = new Post();
        $reply = $post->getHeading($id);
        echo  json_encode($reply['heading']);
    }

    public function edittextAction(){
        $id = $_POST['id'];
        $post = new Post();
        $reply = $post->getText($id);
        echo json_encode($reply['text']);

    }




}