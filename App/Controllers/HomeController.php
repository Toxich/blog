<?php

namespace Blog\App\Controllers;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\Post;
use Blog\App\Models\Preferences;

use Sunra\PhpSimple\HtmlDomParser;

class HomeController extends Controller
{

    public function indexAction($request = null)
    {
        $post = new Post();
        $preferenceObj = new Preferences();
        if(isset($_SESSION['user'])) {
            $preferences = $preferenceObj->getPreferencesList($_SESSION['user']);
            if (!empty($preferences)) {
                foreach ( $preferences as $key){
                    $parentId[] = $key['parent_id'];
                    $preferencesId[] = $key['id'];
                }
                $posts = $post->getPostsSortedByPreferences($parentId);
                $this->view->render([
                                        "home_posts" => $posts,
                                        "all_posts" => $post->getPosts(),
                                        "preferences_posts" => $post->getPostsByPreferences($preferencesId),
                                        "preferences" => $preferences
                                    ]);
                return;
            }else {
            $this->view->render(["posts" => $post->getPosts()]);
        }
        }else {
            $this->view->render(["posts" => $post->getPosts()]);
        }
    }

    public function testAction()
    {
        $html = file_get_contents('https://itc.ua/');
        $domDoc = new \DOMDocument();
        libxml_use_internal_errors(true);
        $domDoc->loadHTML($html);
        $finder = new \DOMXPath($domDoc);
        $classname="category-news";
        $divs = $finder->query("//div[contains(@class,'$classname')]");
        $textsDivs = $finder->query("//div[contains(@class,'$classname')]//div[contains(@class,'entry-excerpt hidden-xs')]");
        $headers_array = [];
        $texts_array = [];
        foreach ($textsDivs as $div){
            $texts_array[] = strval($div->nodeValue);
        }
        foreach ($divs as $div) {
            $heads = $div->getElementsByTagName('h2');
            foreach ($heads as $head){
                $headers_array[] = $head->nodeValue;
            }
        }
        var_dump($headers_array);
        var_dump($texts_array);
        $this->view->render();
    }

}