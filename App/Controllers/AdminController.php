<?php

namespace Blog\App\Controllers;

use Blog\App\Controllers\Base\Controller;
use Blog\App\Models\User;
use Blog\App\Models\Like;
use Blog\App\Models\Post;

class AdminController extends Controller
{

    public function indexAction($request = null)
    {

        if (!isset($_SESSION['admin'])) {
            header("Location: home");
        }
        $post = new Post();
        $like = new Like();
        $posts = $post->getPosts();

        foreach ($posts as &$key) {
            $key['likes'] = count($like->getLikes($key['id']));
        }

//        echo '<pre>';
//        print_r($posts);
//        echo '</pre>';

        $this->view->render(['posts' => $posts]);
    }

    public function postDeleteAction()
    {
        $id = $_POST['postId'];
        $post = new Post();
        $result = $post->delete($id);
        if ($result) {
            echo $result;
        } else {
            echo false;
        }
    }
}