CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` int(11) DEFAULT NULL,
  `hashmail` varchar(255) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `users`(mail, name, password, active, hashmail, admin) VALUES ('toxich.chen@gmail.com', 'Anton', '49ba08e036cb1488e3f1a2fc0c3b21cd08b87018', 1, 'b51b0956a685461b3421fb3feb9563fc95da9c01', 1 );

CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `categories`( parent_id, title ) VALUES (0, 'Music');
INSERT INTO `categories`( parent_id, title ) VALUES (0, 'Art');
INSERT INTO `categories`( parent_id, title ) VALUES (0, 'Games');
INSERT INTO `categories`( parent_id, title ) VALUES (2, 'Rock');
INSERT INTO `categories`( parent_id, title ) VALUES (2, 'Rap');
INSERT INTO `categories`( parent_id, title ) VALUES (2, 'Jazz');
INSERT INTO `categories`( parent_id, title ) VALUES (3, 'Modern');
INSERT INTO `categories`( parent_id, title ) VALUES (3, 'Retro');
INSERT INTO `categories`( parent_id, title ) VALUES (4, 'Shooters');
INSERT INTO `categories`( parent_id, title ) VALUES (4, 'Strategies');
INSERT INTO `categories`( parent_id, title ) VALUES (4, 'Horrors');



CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading` VARCHAR(255) NOT NULL,
  `text` VARCHAR(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;